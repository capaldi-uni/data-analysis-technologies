# Лабораторные работы по Intel DAAL

[Презентации](https://nnov.hse.ru/bipm/project_1)

## Подготовка окружения

> На linux и macOS, используйте pip3 и python3, на случай если python2 установлен по умолчанию

Создайте и активируйте виртуальное окружение (можно использовать pycharm и пропустить эти команды):

    virtualenv venv

    venv\scripts\activate   # windows
    source venv/scripts/activate   # linux/macOS

Вы должны увидеть `(venv)` в начале командной строки:

![prompt](prompt.png)

Затем, установите используемые библиотеки:

    pip install daal4py scikit-learn seaborn notebook

Или, чтобы получить конкретные версии:

    pip install -r requirements.txt

После, создайте ядро (kernel) ipykernel из созданного окружения (здесь `venv` это имя создаваемого ядра и может быть любым валидным именем):

    python -m ipykernel install --user --name=venv

Наконец, запустите jupyter lab:

    jupyter lab

При создании блокнотов используйте новое ядро вместо стандартного.

![kernel](kernel.png)

При открытии файлов `.ipynb` в первый раз, если появляется предложение выбрать ядро, выбирайте новое ядро. Если предложения не появляется, необходимо перейти в меню `Kernel -> Change kernel` и убедиться, что выбрано новое ядро.

![kernel_selected](kernel_selected.png)

